<?
AddEventHandler('main', 'OnEpilog', '_Check404Error', 1);
function _Check404Error(){
    if (defined('ERROR_404') && ERROR_404 == 'Y') {
        global $APPLICATION;
        $APPLICATION->RestartBuffer();
        include $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/header.php';
        include $_SERVER['DOCUMENT_ROOT'] . '/404.php';
        include $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/footer.php';
    }
}

function pp($array)
{
    echo '<pre>';
    print_r($array);
    echo "</pre>";
}

function vd($element)
{
    var_dump($element);
}

AddEventHandler("iblock", "OnIBlockPropertyBuildList", array("CIBlockPropertyPicture", "GetUserTypeDescription"));
AddEventHandler("iblock", "OnBeforeIBlockElementDelete", array("CIBlockPropertyPicture", "OnBeforeIBlockElementDelete"));

AddEventHandler("iblock", "OnBeforeIBlockElementAdd", Array("MyHandlers", "ResizeElementProperty"));
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", Array("MyHandlers", "ResizeElementProperty"));

class MyHandlers
{
	function ResizeElementProperty(&$arFields)
	{

		global $APPLICATION;
		//��� ��������� �������� �������� ��������� � ���������������
		$IBLOCK_ID = 4;

		if($arFields["IBLOCK_ID"] == $IBLOCK_ID)
		{
			if ($arFields["DETAIL_PICTURE"]["size"] == $arFields["PREVIEW_PICTURE"]["size"])
				$arFields["DETAIL_PICTURE"] = array();

		}

	}
}

class CIBlockPropertyPicture
{
   function GetUserTypeDescription()
   {
      return array(
         "PROPERTY_TYPE"      =>"E",
         "USER_TYPE"      =>"Picture",
         "DESCRIPTION"      =>"��������",
         "GetPropertyFieldHtml" =>array("CIBlockPropertyPicture", "GetPropertyFieldHtml"),
         "GetPublicViewHTML" =>array("CIBlockPropertyPicture", "GetPublicViewHTML"),
         "ConvertToDB" =>array("CIBlockPropertyPicture", "ConvertToDB"),

         //"GetPublicEditHTML" =>array("CIBlockPropertyPicture","GetPublicEditHTML"),
         //"GetAdminListViewHTML" =>array("CIBlockPropertyPicture","GetAdminListViewHTML"),
         //"CheckFields" =>array("CIBlockPropertyPicture","CheckFields"),
         //"ConvertFromDB" =>array("CIBlockPropertyPicture","ConvertFromDB"),
         //"GetLength" =>array("CIBlockPropertyPicture","GetLength"),
      );
   }

   function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
   {
      $LINK_IBLOCK_ID = intval($arProperty["LINK_IBLOCK_ID"]);
      if($LINK_IBLOCK_ID)
      {
         $ELEMENT_ID = intval($value["VALUE"]);
         if($ELEMENT_ID)
         {
            $rsElement = CIBlockElement::GetList(array(), array("IBLOCK_ID" => $arProperty["LINK_IBLOCK_ID"], "ID" => $value["VALUE"]), false, false, array("ID", "PREVIEW_PICTURE", "DETAIL_PICTURE"));
            $arElement = $rsElement->Fetch();
            if(is_array($arElement))
               $file_id = $arElement["DETAIL_PICTURE"] ? $arElement["DETAIL_PICTURE"] : ($arElement["PREVIEW_PICTURE"] ? $arElement["PREVIEW_PICTURE"] : 0);
            else
               $file_id = 0;
         }
         else
         {
            $file_id = 0;
         }

         if($file_id)
         {
            $db_img = CFile::GetByID($file_id);
            $db_img_arr = $db_img->Fetch();
            if($db_img_arr)
            {
               $strImageStorePath = COption::GetOptionString("main", "upload_dir", "upload");
               $sImagePath = "/".$strImageStorePath."/".$db_img_arr["SUBDIR"]."/".$db_img_arr["FILE_NAME"];
               return '<label><input name="'.$strHTMLControlName["VALUE"].'[del]" value="Y" type="checkbox"> ������� ���� '.$sImagePath.'</label>'
               .'<input name="'.$strHTMLControlName["VALUE"].'[old]" value="'.$ELEMENT_ID.'" type="hidden">';
            }
         }
         return '<input type="file" size="'.$arProperty["COL_COUNT"].'" name="'.$strHTMLControlName["VALUE"].'"/>';
      }
      else
      {
         return "������ ��������� ��������. ������� �������� � ������� ����� ��������� ��������.";
      }
   }

   function GetPublicViewHTML($arProperty, $value, $strHTMLControlName)
   {
      $LINK_IBLOCK_ID = intval($arProperty["LINK_IBLOCK_ID"]);
      if($LINK_IBLOCK_ID)
      {
         $ELEMENT_ID = intval($value["VALUE"]);
         if($ELEMENT_ID)
         {
            $rsElement = CIBlockElement::GetList(array(), array("IBLOCK_ID" => $arProperty["LINK_IBLOCK_ID"], "ID" => $value["VALUE"]), false, false, array("ID", "PREVIEW_PICTURE", "DETAIL_PICTURE"));
            $arElement = $rsElement->Fetch();
            if(is_array($arElement))
            	if ($arElement["PREVIEW_PICTURE"] && $arElement["DETAIL_PICTURE"])
               		return CFile::Show2Images($arElement["PREVIEW_PICTURE"], $arElement["DETAIL_PICTURE"]);
               	elseif ($arElement["PREVIEW_PICTURE"])
               		return CFile::ShowImage($arElement["PREVIEW_PICTURE"]);
         }
      }
      return "";
   }

   function ConvertToDB($arProperty, $value)
   {
      $arResult = array("VALUE" => "", "DESCRIPTION" => "");
      $LINK_IBLOCK_ID = intval($arProperty["LINK_IBLOCK_ID"]);
      if($LINK_IBLOCK_ID)
      {
         if(
            is_array($value["VALUE"])
            && is_array($value["VALUE"]["error"])
            && $value["VALUE"]["error"]["VALUE"] == 0
            && $value["VALUE"]["size"]["VALUE"] > 0
         )
         {
            $arDetailPicture =  array(
               "name" => $value["VALUE"]["name"]["VALUE"],
               "type" => $value["VALUE"]["type"]["VALUE"],
               "tmp_name" => $value["VALUE"]["tmp_name"]["VALUE"],
               "error" => $value["VALUE"]["error"]["VALUE"],
               "size" => $value["VALUE"]["size"]["VALUE"],
            );
            $obElement = new CIBlockElement;
            $arResult["VALUE"] = $obElement->Add(array(
               "IBLOCK_ID" => $LINK_IBLOCK_ID,
               "NAME" => $arDetailPicture["name"],
               "DETAIL_PICTURE" => $arDetailPicture,
            ), false, false, true);
         }
         elseif(
            is_array($value["VALUE"])
            && isset($value["VALUE"]["size"])
            && !is_array($value["VALUE"]["size"])
            && $value["VALUE"]["size"] > 0
         )
         {
            $arDetailPicture =  array(
               "name" => $value["VALUE"]["name"],
               "type" => $value["VALUE"]["type"],
               "tmp_name" => $value["VALUE"]["tmp_name"],
               "error" => intval($value["VALUE"]["error"]),
               "size" => $value["VALUE"]["size"],
            );
            $obElement = new CIBlockElement;
            $arResult["VALUE"] = $obElement->Add(array(
               "IBLOCK_ID" => $LINK_IBLOCK_ID,
               "NAME" => $arDetailPicture["name"],
               "DETAIL_PICTURE" => $arDetailPicture,
            ), false, false, true);
         }
         elseif($value["VALUE"]["del"])
         {
            $obElement = new CIBlockElement;
            $obElement->Delete($value["VALUE"]["old"]);
         }
         elseif($value["VALUE"]["old"])
         {
            $arResult["VALUE"] = $value["VALUE"]["old"];
         }
         elseif(!is_array($value["VALUE"]) && intval($value["VALUE"]))
         {
            $arResult["VALUE"] = $value["VALUE"];
         }
      }
      return $arResult;
   }

   function OnBeforeIBlockElementDelete($ELEMENT_ID)
   {
      $arProperties = array();
      $rsElement = CIBlockElement::GetList(array(), array("ID" => $ELEMENT_ID), false, false, array("ID", "IBLOCK_ID"));
      $arElement = $rsElement->Fetch();
      if($arElement)
      {
         $rsProperties = CIBlockProperty::GetList(array(), array("IBLOCK_ID" => $arElement["IBLOCK_ID"], "USER_TYPE" => "Picture"));
         while($arProperty = $rsProperties->Fetch())
            $arProperties[] = $arProperty;
      }

      $arElements = array();
      foreach($arProperties as $arProperty)
      {
         $rsPropValues = CIBlockElement::GetProperty($arElement["IBLOCK_ID"], $arElement["ID"], array(
            "EMPTY" => "N",
            "ID" => $arProperty["ID"],
         ));
         while($arPropValue = $rsPropValues->Fetch())
         {
            $ID = intval($arPropValue["VALUE"]);
            if($ID > 0)
               $arElements[$ID] = $ID;
         }
      }

      foreach($arElements as $to_delete)
      {
         CIBlockElement::Delete($to_delete);
      }
   }
}
AddEventHandler("main", "OnBeforeProlog", "MyOnBeforePrologHandler", 50);

function MyOnBeforePrologHandler()
{
   $url = $_SERVER['REQUEST_URI'];
   if($url == '/actives/')
   {
      header('HTTP/1.1 301 Moved Permanently');
      header('Location: http://vebcapital.ru/consultancy/');
      exit();
   }
   else if($url == '/about/dolevoefin/' || $url == '//about/dolevoefin/')
   {
      header('HTTP/1.1 301 Moved Permanently');
      header('Location: http://vebcapital.ru/');
      exit();
   }
}
?>