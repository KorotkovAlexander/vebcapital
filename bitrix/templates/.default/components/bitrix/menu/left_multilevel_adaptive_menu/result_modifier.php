<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/**
 * @var array $arParams
 * @var array $arResult
 * @var CMain $APPLICATION
 * @var CUser $USER
 * @var CDatabase $DB
 * @var CBitrixComponentTemplate $this
 * @var string $componentPath
 * @var CBitrixComponent $component
 */

if (empty($arResult)) {
    return;
}

class Functions
{
    /**
     * Преобразует меню в виде nested sets в древовидную структуру
     *
     * @param array $items
     * @param int   $depthLevel
     *
     * @return array
     */
    public static function buildMenuTree(array $items, $depthLevel = 1)
    {
        $result = [];

        $offset = 0;
        $continueCount = 0;
        foreach ($items as $key => $item) {
            $offset++;
            if ($continueCount > 0) {
                $continueCount--;
                continue;
            }
            if ($item['IS_PARENT']) {
                $newItems = array_slice($items, $offset);
                $item['CHILDS'] = self::buildMenuTree($newItems, reset($newItems)['DEPTH_LEVEL']);
                $continueCount = count($item['CHILDS']);
            }
            if ($item['DEPTH_LEVEL'] < $depthLevel) {
                break;
            }
            $result[$key] = $item;
        }

        return $result;
    }
}

$arResult['TREE_MENU'] = Functions::buildMenuTree($arResult);



