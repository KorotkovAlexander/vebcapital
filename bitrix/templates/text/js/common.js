$( document ).ready(function() {
    /*Фильтр для раздела press*/
    (function() {
        [].slice.call( document.querySelectorAll( 'select.cs-select' ) ).forEach( function(el) {
            new SelectFx(el);
        } );
    })();

    $('#selectmoth .cs-options ul li').on('click', function() {
        var moth = $(this).attr("data-value"),

        url = delPrm(document.location.href, "moth");
        url = setAttr("moth", moth, url);

        if ($("li.fed-tab").hasClass("active")) {
            url = delPrm(url, "fed", url);
            url = delPrm(url, "act", url);
            url = delPrm(url, "etc", url);
            url = setAttr("fed", "true", url);
        }
        if ($("li.act-tab").hasClass("active")) {
            url = delPrm(url, "fed", url);
            url = delPrm(url, "act", url);
            url = delPrm(url, "etc", url);
            url = setAttr("act", "true", url);
        }
        if ($("li.etc-tab").hasClass("active")) {
            url = delPrm(url, "fed", url);
            url = delPrm(url, "act", url);
            url = delPrm(url, "etc", url);
            url = setAttr("etc", "true", url);
        }

        $(location).attr('href',url);
    });

    $('#selectyear .cs-options ul li').on('click', function() {
        var moth = $(this).attr("data-value"),

        url = delPrm(document.location.href, "year");
        url = setAttr("year", moth, url);

        if ($("li.video-tab").hasClass("active")) {
            url = delPrm(url, "video");
            url = setAttr("video", "true", url);
        } else {
            url = delPrm(url, "video");
        }

        $(location).attr('href',url);
    });

    function setAttr(prmName,val, d){
        var res = '';
        var d = d.split("#")[0].split("?");
        var base = d[0];
        var query = d[1];
        if(query) {
            var params = query.split("&");
            for(var i = 0; i < params.length; i++) {
                var keyval = params[i].split("=");
                if(keyval[0] != prmName) {
                    res += params[i] + '&';
                }
            }
        }
        res += prmName + '=' + val;
        //window.location.href = base + '?' + res;
        return base + '?' + res;
    }

    function delPrm(Url,Prm) {
        var a=Url.split('?');
        var re = new RegExp('(\\?|&)'+Prm+'=[^&]+','g');
        Url=('?'+a[1]).replace(re,'');
        Url=Url.replace(/^&|\?/,'');
        var dlm=(Url=='')? '': '?';
        return a[0]+dlm+Url;
    };

    /*Переключение табов*/
    $('ul.tabs__caption').on('click', 'li:not(.active)', function() {
        $(this)
            .addClass('active').siblings().removeClass('active')
            .closest('div.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
    });
});

/*Модальное окно*/
$(document).ready(function() { // вся мaгия пoсле зaгрузки стрaницы
    $('a.go-modal').click( function(event){ // лoвим клик пo ссылки с id="go"
        event.preventDefault(); // выключaем стaндaртную рoль элементa

        $.ajax({
            type: "POST",
            url: "/press/video/ajax.php",
            data: { url_video: $(this).attr("data-video") }
        }).done(function( content ) {
            $(".modal-video").html(content);
        });

        $('#overlay').fadeIn(400, // снaчaлa плaвнo пoкaзывaем темную пoдлoжку
            function(){ // пoсле выпoлнения предъидущей aнимaции
                $('#modal_form')
                    .css('display', 'block') // убирaем у мoдaльнoгo oкнa display: none;
                    .animate({opacity: 1, top: '50%'}, 200); // плaвнo прибaвляем прoзрaчнoсть oднoвременнo сo съезжaнием вниз
            });
    });
    /* Зaкрытие мoдaльнoгo oкнa, тут делaем тo же сaмoе нo в oбрaтнoм пoрядке */
    $('#modal_close, #overlay').click( function(){ // лoвим клик пo крестику или пoдлoжке
        jQuery(".iframe-video").each(function() {
            jQuery(this)[0].contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*')
        });
        $('#modal_form')
            .animate({opacity: 0, top: '45%'}, 200,  // плaвнo меняем прoзрaчнoсть нa 0 и oднoвременнo двигaем oкнo вверх
                function(){ // пoсле aнимaции
                    $(this).css('display', 'none'); // делaем ему display: none;
                    $('#overlay').fadeOut(400); // скрывaем пoдлoжку
                }
            );
    });
});

/*Увеличение картинок в тексте*/

    $(document).ready(function() { // Ждём загрузки страницы

        $(".minimized").click(function(){	// Событие клика на маленькое изображение
            var img = $(this);	// Получаем изображение, на которое кликнули
            var src = img.attr('src'); // Достаем из этого изображения путь до картинки
            $("body").append("<div class='popup'>"+ //Добавляем в тело документа разметку всплывающего окна
                "<div class='popup_bg'></div>"+ // Блок, который будет служить фоном затемненным
                "<img src='"+src+"' class='popup_img' />"+ // Само увеличенное фото
                "</div>");
            $(".popup").fadeIn(800); // Медленно выводим изображение
            $(".popup_bg").click(function(){	// Событие клика на затемненный фон
                $(".popup").fadeOut(200);	// Медленно убираем всплывающее окно
                setTimeout(function() {	// Выставляем таймер
                    $(".popup").remove(); // Удаляем разметку всплывающего окна
                }, 200);
            });
        });

    });

/*----------------------------*/

/*Выбор вкладок на странице реализация имущества*/
$(document).ready(function() { // Ждём загрузки страницы

    $('body').on('click', '.cs-options ul li', function () {

        let curTab = $(this).attr("data-value");

        $('.tabs__content').each(function(i,elem) {
            $(this).removeClass("active");
            if ($(this).attr("data-tab") == curTab) {
                $(this).addClass("active");
            }
        })
    });

});
/*----------------------------------------------*/

/*Раскрываем подпункты в меню*/
$(document).ready(function() { // Ждём загрузки страницы
    $('body').on('click', '.child_menu_link', function (e) {
        e.preventDefault();
        let childList = $(this).attr("data-child");
        if ($(this).hasClass('active')) {
            $(this).toggleClass('active');
            $('ul.child_menu_list[data-child="' + childList + '"]').hide(400);
            $(this).find('img').attr('src', '/bitrix/templates/index/images/icon-down.png');
        } else {
            $(this).toggleClass('active');
            $('ul.child_menu_list[data-child="' + childList + '"]').show(400);
            $(this).find('img').attr('src', '/bitrix/templates/index/images/icon-up.png');
        }
    });

    var iOS = navigator.userAgent.match(/iPhone|iPad|iPod/i);

    if(iOS != null) {
        $('.child_menu_link').touchstart(function (e) {
            let childList = $(this).attr("data-child");
            if ($(this).hasClass('active')) {
                $(this).toggleClass('active');
                $('ul.child_menu_list[data-child="' + childList + '"]').hide(400);
                $(this).find('img').attr('src', '/bitrix/templates/index/images/icon-down.png');
            } else {
                $(this).toggleClass('active');
                $('ul.child_menu_list[data-child="' + childList + '"]').show(400);
                $(this).find('img').attr('src', '/bitrix/templates/index/images/icon-up.png');
            }
            e.preventDefault();
        });
    }
});
/*---------------------------*/