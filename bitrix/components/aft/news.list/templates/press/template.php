<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<style type="text/css">
.years-menu{
  margin: 0 20px 35px 0px;
  height: 80px;
}
.years-menu li{
  color: #fff;
  float: left;
  font-size: 29px;
  margin: 0;
  width: 20%;
}
.years-menu li a{
    border: 1px solid #fff;
    color: #fff;
    display: block;
    margin: 0 auto;
    padding: 30px 0;
    text-align: center;
    text-decoration: none;
    width: 116px;
}
.years-menu li.noactiv a{
  border: 1px solid #6FA287;
  background: #fff!important;
  color: #6FA287;
}

.years-menu li.green-d a{
  background: #669d80;
}
.years-menu li.blue a{
  background: #6FA287;
}
.years-menu li.viol a{
  background: #8e83bd;
}
.years-menu li.orange a{
  background: #f9ad62;
}
.years-menu li.green-l a{
  background: #70c87c;
}
.years-menu li.green-l {
   margin: 0;
}
</style>

<div class="news-list">
    <?if($arParams["DISPLAY_TOP_PAGER"]):?>
        <?=$arResult["NAV_STRING"]?><br />
    <?endif;?>
    <?foreach($arResult["ITEMS"] as $arItem):?>
        <p class="news-item">
            <?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
                <?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
                    <a href="<?echo $arItem["DETAIL_PAGE_URL"]?>"><b><?echo $arItem["NAME"]?></b></a><br />
                <?else:?>
                    <b><?echo $arItem["NAME"]?></b><br /><br />
                <?endif;?>
            <?endif;?>
            <?if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):?>
                <span class="news-date-time"><?echo $arItem["DISPLAY_ACTIVE_FROM"]?></span>
            <?endif?>
            <?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
                <br><br><?echo $arItem["PREVIEW_TEXT"];?>
            <?endif;?>
            <?foreach($arItem["FIELDS"] as $code=>$value):?>
                <small>
                <?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?=$value;?>
                </small><br />
            <?endforeach;?>
            <?foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
                <small>
                <?=$arProperty["NAME"]?>:&nbsp;
                <?if(is_array($arProperty["DISPLAY_VALUE"])):?>
                    <?=implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);?>
                <?else:?>
                    <?=$arProperty["DISPLAY_VALUE"];?>
                <?endif?>
                </small><br />
            <?endforeach;?>

            <br><div class="read-more"><a href="<?=$arItem["DETAIL_PAGE_URL"];?>">Подробнее</a></div>

        </p>
    <?endforeach;?>

    <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
        <br /><?=$arResult["NAV_STRING"]?>
    <?endif;?>
</div>
