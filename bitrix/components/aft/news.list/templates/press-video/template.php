<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="video-list">
    <?foreach($arResult["ITEMS"] as $arItem):?>
        <div class="video-list__item">
            <?$urlvideo = str_replace("watch?v=", "embed/", $arItem["PROPERTIES"]["URL_VIDEO"]["VALUE"]);?>
            <?if ($arItem["PREVIEW_PICTURE"]["SRC"]) {?>
                <a href="" class="go-modal" data-video="<?=$urlvideo."?enablejsapi=1&"?>">
                    <img src="<?=$arItem["PREVIEW_PICTURE_SRC"]?>">
                </a>
            <?}?>
            <div class="video-list__date">
                <?echo $arItem["DISPLAY_ACTIVE_FROM"]?>
            </div>
            <div class="video-list__name">
                <a href="" class="go-modal" data-video="<?=$urlvideo."?enablejsapi=1&"?>"><?echo $arItem["NAME"]?></a>
            </div>
        </div>
    <?endforeach;?>
</div>

<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
    <br /><?=$arResult["NAV_STRING"]?>
<?endif;?>

<div id="modal_form">
    <span id="modal_close">X</span>
    <div class="modal-video"></div>
</div>
<div id="overlay"></div>