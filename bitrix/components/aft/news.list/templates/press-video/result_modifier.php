<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
foreach ($arResult["ITEMS"] as $key => $arItem) {
    $arPhotoSmall = CFile::ResizeImageGet(
        $arItem["PREVIEW_PICTURE"],
        array("width" => 440, "height" => 244),
        BX_RESIZE_IMAGE_EXACT, true
    );

    $arResult["ITEMS"][$key]["PREVIEW_PICTURE_SRC"] = $arPhotoSmall["src"];
}
?>