<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?

$menuYears = array(
    '2012' => array('activ' => 'no', 'color' => 'green-d'),
    '2013' => array('activ' => 'no', 'color' => 'blue'),
    '2014' => array('activ' => 'no', 'color' => 'viol'),
    '2015' => array('activ' => 'no', 'color' => 'orange'),
    '2016' => array('activ' => 'no', 'color' => 'green-l')
);

foreach ($menuYears as $year => $value) {


    $arFilter = array (
        "IBLOCK_ID" => $arResult["ID"],
        "IBLOCK_LID" => SITE_ID,
        "ACTIVE" => "Y",
        "CHECK_PERMISSIONS" => "Y",
        "PROPERTY_PRESS_YEAR_VALUE" => $year
    );

    $res = CIBlockElement::GetList(Array(), $arFilter, false, false);
    $cnt = $res->SelectedRowsCount();

    $groupYears[$year] = $res->SelectedRowsCount();
}

foreach ($groupYears as $year => $val){
    if($val > 0){
        $menuYears[$year]['activ'] = 'yes';
    }
}

$arResult['YEARS_MENU'] = $menuYears;


?>