<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<style type="text/css">
.years-menu{
  margin: 0 20px 35px 0px;
  height: 80px;
}
.years-menu li{
  color: #fff;
  float: left;
  font-size: 29px;
  margin: 0;
  width: 20%;
}
.years-menu li a{
    border: 1px solid #fff;
    color: #fff;
    display: block;
    margin: 0 auto;
    padding: 30px 0;
    text-align: center;
    text-decoration: none;
    width: 116px;
}
.years-menu li.noactiv a{
  border: 1px solid #6FA287;
  background: #fff!important;
  color: #6FA287;
}

.years-menu li.green-d a{
  background: #669d80;
}
.years-menu li.blue a{
  background: #6FA287;
}
.years-menu li.viol a{
  background: #8e83bd;
}
.years-menu li.orange a{
  background: #f9ad62;
}
.years-menu li.green-l a{
  background: #70c87c;
}
.years-menu li.green-l {
   margin: 0;
}
</style>

<div class="news-anons">
    <?if($arParams["DISPLAY_TOP_PAGER"]):?>
        <?=$arResult["NAV_STRING"]?><br />
    <?endif;?>
    <? $i = 0;?>
    <?foreach($arResult["ITEMS"] as $arItem):?>
        <? $i++;?>
        <div class="columns<? if($i == 2) print '-two';?>">
            <div class="new-box">
                <div class="n-title"><a href="<?echo $arItem["DETAIL_PAGE_URL"]?>"><?echo $arItem["NAME"]?></a></div>
                <div class="n-text"><?echo $arItem["PREVIEW_TEXT"];?>
                </div>
            </div>
        </div>

    <?endforeach;?>
    <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
        <br /><?=$arResult["NAV_STRING"]?>
    <?endif;?>
</div>