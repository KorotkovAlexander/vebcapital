<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("���������� ���������");
?>

<?
if (($_GET["moth"] or $_GET["year"]) and $_GET["year"] != "���") {
    global $arrFilter;
    $year = "2018";
    $moth = false;
    if ($_GET["year"]) {
        $year = $_GET["year"];
    }
    if ($_GET["moth"]) {
        $moth = $_GET["moth"];
        switch ($moth) {
            case "������":
                $moth = 1;
                break;
            case "�������":
                $moth = 2;
                break;
            case "����":
                $moth = 3;
                break;
            case "������":
                $moth = 4;
                break;
            case "���":
                $moth = 5;
                break;
            case "����":
                $moth = 6;
                break;
            case "����":
                $moth = 7;
                break;
            case "������":
                $moth = 8;
                break;
            case "��������":
                $moth = 9;
                break;
            case "�������":
                $moth = 10;
                break;
            case "������":
                $moth = 11;
                break;
            case "�������":
                $moth = 12;
                break;
            case "���":
                $moth = false;
                break;
        }

    }

    if ($moth and !$year) {
        $firstMonth  = '01.' . $moth . '.' . $year;
        if ((int)$moth == 12) {
            $moth = 1;
            $year = (int)$year + 1;
        } else {
            $moth = (int)$moth + 1;
        }
        $lastMonth = '01.' . $moth . '.' . $year;
    }
    if (!$moth and $year) {
        $firstMonth = '01.01.'.$year; //������ ����
        $lastMonth = '01.12.'.$year; //����� ����
    }
    if ($moth and $year) {
        $firstMonth  = '01.' . $moth . '.' . $year;
        if ((int)$moth == 12) {
            $moth = 1;
            $year = (int)$year + 1;
        } else {
            $moth = (int)$moth + 1;
        }
        $lastMonth = '01.' . $moth . '.' . $year;
    }

    $arrFilter = array(
        "LOGIC" => "AND",
        array(">=DATE_ACTIVE_FROM" => ConvertTimeStamp(strtotime($firstMonth),"FULL")),
        array("<=DATE_ACTIVE_FROM" => ConvertTimeStamp(strtotime($lastMonth),"FULL")),
    );
}

if ($_GET["fed"]) {
    $fed = "active";
    $act = "";
    $etc = "";
    $all = "";
} elseif ($_GET["act"]) {
    $fed = "";
    $act = "active";
    $etc = "";
    $all = "";
} elseif ($_GET["etc"]) {
    $fed = "";
    $act = "";
    $etc = "active";
    $all = "";
} else {
    $fed = "";
    $act = "";
    $etc = "";
    $all = "active";
}
?>


<div class="tabs">
    <div class="relaliz-description">
        <p>��� ���� ������� ����������� �� ����� ���������� ��������� ������������ ������� ���������������� � �������������� ��������� � ������ ������������.</p>

        <p>�������� ��������� ������ �� ���������� ������������ �������. ������ � ��������������.</p>

        <p>����������� �������� ����������� � �� �������: <a href="http://vebtrade.ru/">www.vebtrade.ru</a></p>
    </div>
    <ul class="tabs__caption">
        <li class="all-tab <?=$all?>">���</li>
        <li class="fed-tab <?=$fed?>">����������� ���������</li>
        <li class="act-tab <?=$act?>">������ ������ ���</li>
        <li class="etc-tab <?=$etc?>">������ ������</li>
    </ul>
    <div style="clear: both"></div>

    <div class="press__content">

        <div class="news-filter">
            <div id="selectmoth">
                <section>
                    <select class="cs-select cs-skin-elastic" id="selectmoth">
                        <?if ($_GET["moth"]) {?>
                            <option value="" disabled selected><?=$_GET["moth"]?></option>
                        <?} else {?>
                            <option value="" disabled selected>�����</option>
                        <?}?>
                        <option>���</option>
                        <option>������</option>
                        <option>�������</option>
                        <option>����</option>
                        <option>������</option>
                        <option>���</option>
                        <option>����</option>
                        <option>����</option>
                        <option>������</option>
                        <option>��������</option>
                        <option>�������</option>
                        <option>������</option>
                        <option>�������</option>
                    </select>
                </section>
            </div>
            <div id="selectyear">
                <section>
                    <select class="cs-select cs-skin-elastic">
                        <?if ($_GET["year"]) {?>
                            <option value="" disabled selected><?=$_GET["year"]?></option>
                        <?} else {?>
                            <option value="" disabled selected>���</option>
                        <?}?>
                        <option>���</option>
                        <option>2018</option>
                        <option>2017</option>
                        <option>2016</option>
                        <option>2015</option>
                        <option>2014</option>
                        <option>2013</option>
                    </select>
                </section>
            </div>
        </div>

        <div class="tabs__content <?=$all?>">
            <?/*������ ������ ���*/?>
            <?$APPLICATION->IncludeComponent("aft:news.list", "realization", array(
                "IBLOCK_TYPE" => "object",
                "IBLOCK_ID" => 15,
                "NEWS_COUNT" => "5",
                "SORT_BY1" => "ACTIVE_FROM",
                "SORT_ORDER1" => "DESC",
                "SORT_BY2" => "SORT",
                "SORT_ORDER2" => "ASC",
                "FILTER_NAME" => "",
                "FIELD_CODE" => array(
                    0 => "",
                    1 => "",
                ),
                "PROPERTY_CODE" => array(
                    0 => "FED_PROP",
                    1 => "ACT_PROP",
                    2 => "ETC_PROP",
                ),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_SHADOW" => "Y",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "3600",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "Y",
                "PREVIEW_TRUNCATE_LEN" => "",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "DISPLAY_PANEL" => "N",
                "SET_TITLE" => "Y",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "Y",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => $_REQUEST["SECTION_ID"],
                "PARENT_SECTION_CODE" => $_REQUEST["SECTION_CODE"],
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "PAGER_TITLE" => "�����-�����",
                "PAGER_SHOW_ALWAYS" => "Y",
                "PAGER_TEMPLATE" => "news",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "Y",
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "AJAX_OPTION_ADDITIONAL" => ""
            ),
                false
            );?>
        </div>

        <div class="tabs__content <?=$fed?>">
            <?/*����������� ���������*/?>
            <?$arrFilter["7919"] = array("PROPERTY_FED_PROP_VALUE" => "Y");?>
            <?$APPLICATION->IncludeComponent("aft:news.list", "realization", array(
                "IBLOCK_TYPE" => "object",
                "IBLOCK_ID" => 15,
                "NEWS_COUNT" => "5",
                "SORT_BY1" => "ACTIVE_FROM",
                "SORT_ORDER1" => "DESC",
                "SORT_BY2" => "SORT",
                "SORT_ORDER2" => "ASC",
                "FILTER_NAME" => "arrFilter",
                "FIELD_CODE" => array(
                    0 => "",
                    1 => "",
                ),
                "PROPERTY_CODE" => array(
                    0 => "FED_PROP",
                    1 => "ACT_PROP",
                    2 => "ETC_PROP",
                ),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_SHADOW" => "Y",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "3600",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "Y",
                "PREVIEW_TRUNCATE_LEN" => "",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "DISPLAY_PANEL" => "N",
                "SET_TITLE" => "Y",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "Y",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => $_REQUEST["SECTION_ID"],
                "PARENT_SECTION_CODE" => $_REQUEST["SECTION_CODE"],
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "PAGER_TITLE" => "�����-�����",
                "PAGER_SHOW_ALWAYS" => "Y",
                "PAGER_TEMPLATE" => "news",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "Y",
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "AJAX_OPTION_ADDITIONAL" => ""
            ),
                false
            );?>
        </div>

        <div class="tabs__content <?=$act?>">
            <?/*������ ������ ���*/?>
            <?$arrFilter["7919"] = array("PROPERTY_ACT_PROP_VALUE" => "Y");?>
            <?$APPLICATION->IncludeComponent("aft:news.list", "realization", array(
                "IBLOCK_TYPE" => "object",
                "IBLOCK_ID" => 15,
                "NEWS_COUNT" => "5",
                "SORT_BY1" => "ACTIVE_FROM",
                "SORT_ORDER1" => "DESC",
                "SORT_BY2" => "SORT",
                "SORT_ORDER2" => "ASC",
                "FILTER_NAME" => "arrFilter",
                "FIELD_CODE" => array(
                    0 => "",
                    1 => "",
                ),
                "PROPERTY_CODE" => array(
                    0 => "FED_PROP",
                    1 => "ACT_PROP",
                    2 => "ETC_PROP",
                ),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_SHADOW" => "Y",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "3600",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "Y",
                "PREVIEW_TRUNCATE_LEN" => "",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "DISPLAY_PANEL" => "N",
                "SET_TITLE" => "Y",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "Y",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => $_REQUEST["SECTION_ID"],
                "PARENT_SECTION_CODE" => $_REQUEST["SECTION_CODE"],
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "PAGER_TITLE" => "�����-�����",
                "PAGER_SHOW_ALWAYS" => "Y",
                "PAGER_TEMPLATE" => "news",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "Y",
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "AJAX_OPTION_ADDITIONAL" => ""
            ),
                false
            );?>
        </div>

        <div class="tabs__content <?=$etc?>">
            <?/*������ ������*/?>
            <?$arrFilter["7919"] = array("PROPERTY_ETC_PROP_VALUE" => "Y");?>
            <?$APPLICATION->IncludeComponent("aft:news.list", "realization", array(
                "IBLOCK_TYPE" => "object",
                "IBLOCK_ID" => 15,
                "NEWS_COUNT" => "5",
                "SORT_BY1" => "ACTIVE_FROM",
                "SORT_ORDER1" => "DESC",
                "SORT_BY2" => "SORT",
                "SORT_ORDER2" => "ASC",
                "FILTER_NAME" => "arrFilter",
                "FIELD_CODE" => array(
                    0 => "",
                    1 => "",
                ),
                "PROPERTY_CODE" => array(
                    0 => "FED_PROP",
                    1 => "ACT_PROP",
                    2 => "ETC_PROP",
                ),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_SHADOW" => "Y",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "3600",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "Y",
                "PREVIEW_TRUNCATE_LEN" => "",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "DISPLAY_PANEL" => "N",
                "SET_TITLE" => "Y",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "Y",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => $_REQUEST["SECTION_ID"],
                "PARENT_SECTION_CODE" => $_REQUEST["SECTION_CODE"],
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "PAGER_TITLE" => "�����-�����",
                "PAGER_SHOW_ALWAYS" => "Y",
                "PAGER_TEMPLATE" => "news",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "Y",
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "AJAX_OPTION_ADDITIONAL" => ""
            ),
                false
            );?>
        </div>

    </div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>