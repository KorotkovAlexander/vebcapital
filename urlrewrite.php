<?
$arUrlRewrite = array(
	array(
		"CONDITION"	=>	"#^/reporting/([0-9]+)/(\\?clear_cache=Y)*$#",
		"RULE"	=>	"YEAR=$1",
		"ID"	=>	"",
		"PATH"	=>	"/reporting/index.php",
	),
	array(
		"CONDITION"	=>	"#^/en/press/year/([\\d]+)/.*#",
		"RULE"	=>	"YEAR=$1",
		"ID"	=>	"",
		"PATH"	=>	"/en/press/index.php",
	),
	array(
		"CONDITION"	=>	"#^/privatization/([\\d]+)/.*#",
		"RULE"	=>	"ELEMENT_ID=$1",
		"ID"	=>	"",
		"PATH"	=>	"/privatization/detail.php",
	),
	array(
		"CONDITION"	=>	"#^/press/year/([\\d]+)/.*#",
		"RULE"	=>	"YEAR=$1",
		"ID"	=>	"",
		"PATH"	=>	"/press/index.php",
	),
	array(
		"CONDITION"	=>	"#^/en/press/([\\d]+)/.*#",
		"RULE"	=>	"ELEMENT_ID=$1",
		"ID"	=>	"",
		"PATH"	=>	"/en/press/detail.php",
	),
	array(
		"CONDITION"	=>	"#^/press/([\\d]+)/.*#",
		"RULE"	=>	"ELEMENT_ID=$1",
		"ID"	=>	"",
		"PATH"	=>	"/press/detail.php",
	),
	array(
		"CONDITION"	=>	"#^/en/about/news/#",
		"RULE"	=>	"",
		"ID"	=>	"bitrix:news",
		"PATH"	=>	"/en/about/news/index.php",
	),
	array(
		"CONDITION"	=>	"#^/about/press/#",
		"RULE"	=>	"",
		"ID"	=>	"bitrix:news",
		"PATH"	=>	"/about/press/index.php",
	),
	array(
		"CONDITION"	=>	"#^/about/news/#",
		"RULE"	=>	"",
		"ID"	=>	"bitrix:news",
		"PATH"	=>	"/about/news/index.php",
	),
	array(
		"CONDITION"	=>	"#^/realty/#",
		"RULE"	=>	"",
		"ID"	=>	"bitrix:news",
		"PATH"	=>	"/realty/index.php",
	),
	array(
		"CONDITION"	=>	"#^/#",
		"RULE"	=>	"",
		"ID"	=>	"bitrix:news",
		"PATH"	=>	"/realization/index.php",
	),
);

?>