<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("������ ���.��");
?> 
<p aligh="justify">���.�� - ���������� �������� �������� � ������ 
  <br />
 
  <br />
 ��������������� ���������� �������� &laquo;���.��&raquo; ��������� �� ��������� <a href="https://���.��/o-banke/normativno-pravovaya-baza/" target="_blank" >������������ ������</a> �� ��������������� ���������� �������� ����.�Ի. 
  <br />
 
  <br />
 �������� ������ ���.�� &ndash; �������������� ������������� �������������� �������� ������. � ����������� � ������������� ������� ���.�� ���������� ��������������� ���������� ��������, ������������ �� �������� ��������������, ��������������, ���������� �����, ���������� ���������������� ���������� � ��������� �������� ����� �����. 
  <br />
 
  <br />
 ������ ������� ���������� ���.�� �������� �������������� �����, ������� ����������� ������������ ������������� ���������� ���������. 
  <br />
 
  <br />
 �������� ����������� � ���������� �������� ���.�� ��������� � ���������� ������������ ��������, ������������ �� ���������� �������� ����� ����������. � ����� ��������� ������������ � ���������� ����������� � ������������� �������, ����������, �������������� ��� �������������� ��������. 
  <br />
 
  <br />
 �� ��������� ������� ������������� ���������� ��������� ���.�� ������������ ������������ ���������� �������� �� �������� ����������� ������������� ���������-�������������� �������� ������.</p>
 
<!--<p class="MsoNormal" style="text-align: justify; text-indent: 35.45pt; line-height: 150%;"><span style="font-size: 10pt; line-height: 150%; font-family: Arial, sans-serif;">��������������� ���������� &laquo;���� �������� � ������������������� ������������ (��������������)&raquo; ��������� �� ��������� ������������ ������ �� 17 ��� 2007 ���� � 82-�� �� <span style="color: rgb(31, 73, 125);">�</span>���� ���������. �������������� -������������ ���� ��������, ���� �� ���������� ���������� ���������� ������. �������� ����������� � ���������� ��� ������������ ���������� ������������ � ���������� ��������, ������������ ������������� ������������� ���������� ��������� �� 27 ���� 2007 ���� � 1007-�. ������ ������� ���������� ��������������� �������� �������������� �����, ������� ���������� �������� ����������� ������������ �����, ������ �� ����������� � ���������� ��������. ������������ ��������������� ������ ��������������� - ������������ ������������� ���������� ��������� ������� ��������. � ������ ��������������� ������ �� ��������� ������ ������������ ��������������� ������ ���������� �������.<o:p></o:p></span></p>
 
<p class="MsoNormal" style="text-align: justify; text-indent: 35.45pt; line-height: 150%;"><span style="font-size: 10pt; line-height: 150%; font-family: Arial, sans-serif;">�������������� ���������� ����� ��������� �������� �� 2021 ����, ������� ������������ ������ ������������ ��������� �������� �� ��������� ������������ ��� ��������� ������������:<o:p></o:p></span></p>
 
<p class="MsoListParagraph" style="margin-left: 71.45pt; text-align: justify; text-indent: -18pt; line-height: 150%;"><span style="font-size: 10pt; line-height: 150%; font-family: Symbol;">&middot;<span class="Apple-tab-span" style="white-space: pre;">	</span></span><span style="font-size: 10pt; line-height: 150%; font-family: Arial, sans-serif;">�������������� �������� ������� ��������� � ��������������;<o:p></o:p></span></p>
 
<p class="MsoListParagraph" style="margin-left: 71.45pt; text-align: justify; text-indent: -18pt; line-height: 150%;"><span style="font-size: 10pt; line-height: 150%; font-family: Symbol;">&middot;<span class="Apple-tab-span" style="white-space: pre;">	</span></span><span style="font-size: 10pt; line-height: 150%; font-family: Arial, sans-serif;">�������� ��������������;<o:p></o:p></span></p>
 
<p class="MsoListParagraph" style="margin-left: 71.45pt; text-align: justify; text-indent: -18pt; line-height: 150%;"><span style="font-size: 10pt; line-height: 150%; font-family: Symbol;">&middot;<span class="Apple-tab-span" style="white-space: pre;">	</span></span><span style="font-size: 10pt; line-height: 150%; font-family: Arial, sans-serif;">��������� ����������� ��������; <o:p></o:p></span></p>
 
<p class="MsoListParagraph" style="margin-left: 71.45pt; text-align: justify; text-indent: -18pt; line-height: 150%;"><span style="font-size: 10pt; line-height: 150%; font-family: Symbol;">&middot;<span class="Apple-tab-span" style="white-space: pre;">	</span></span><span style="font-size: 10pt; line-height: 150%; font-family: Arial, sans-serif;">���������� �������� �������� ��������� ���������� � �����������; <o:p></o:p></span></p>
 
<p class="MsoListParagraph" style="margin-left: 71.45pt; text-align: justify; text-indent: -18pt; line-height: 150%;"><span style="font-size: 10pt; line-height: 150%; font-family: Symbol;">&middot;<span class="Apple-tab-span" style="white-space: pre;">	</span></span><span style="font-size: 10pt; line-height: 150%; font-family: Arial, sans-serif;">��������� ��������� � �������� ������������ ��������������� ����������. <o:p></o:p></span></p>
 
<p class="MsoNormal" style="text-align: justify; text-indent: 35.45pt; line-height: 150%;"><span style="font-size: 10pt; line-height: 150%; font-family: Arial, sans-serif;">��� ���������� ����������� ������� ��������, ��������������� ����������� �����������, ������� ����������� ������-������� ������ � ���������, ����� ����������� ��� �������� � �������������� �������� ����������.<o:p></o:p></span></p>
 
<p class="MsoNormal" style="text-align: justify; text-indent: 35.45pt; line-height: 150%;"><span style="font-size: 10pt; line-height: 150%; font-family: Arial, sans-serif;">���� ������� ����������� �������������� � ��������� ���������� �� ������������, � ������� � ������ ���� ������� ������������ �������, �������:<o:p></o:p></span></p>
 
<p class="MsoListParagraph" style="margin-left: 71.45pt; text-align: justify; text-indent: -18pt; line-height: 150%;"><span style="font-size: 10pt; line-height: 150%; font-family: Symbol;">&middot;<span class="Apple-tab-span" style="white-space: pre;">	</span></span><span style="font-size: 10pt; line-height: 150%; font-family: Arial, sans-serif;">�������� �������� ��������� � ��������� 4.0. ������� ����������� ����������� ��� ������� ��������������, ����� ��������� � �������� � ������������� ����� ������������������ �����;<o:p></o:p></span></p>
 
<p class="MsoListParagraph" style="margin-left: 71.45pt; text-align: justify; text-indent: -18pt; line-height: 150%;"><span style="font-size: 10pt; line-height: 150%; font-family: Symbol;">&middot;<span class="Apple-tab-span" style="white-space: pre;">	</span></span><span style="font-size: 10pt; line-height: 150%; font-family: Arial, sans-serif;">�������� ��������� �� ������ ���������� ��������. ��������� ������� �������� ������������� ������ �������� � ���������� ��-������� ��� ��������, ����������� ������, ��������, ������� ����������;</span></p>
 
<p class="MsoListParagraph" style="margin-left: 71.45pt; text-align: justify; text-indent: -18pt; line-height: 150%;"><span style="font-size: 10pt; line-height: 150%; font-family: Symbol;">&middot;<span class="Apple-tab-span" style="white-space: pre;">	</span></span><span style="font-size: 10pt; line-height: 150%; font-family: Arial, sans-serif;">��������������� �� ������ ������������� ����������.<o:p></o:p></span></p>-->
 
<p>����: <a target="_blank" href="http://www.veb.ru/" >www.veb.ru</a> </p>
 
<table border="0" align="center" cellpadding="0" cellspacing="0"> 
  <tbody> 
    <tr>
        <td style="border-image: initial;" rowspan="2" width="80px;" align="center" valign="middle">&nbsp;<img src="/upload/news/ik1.svg" style="min-width: 125%;" /></td>
        <td valign="bottom" style="border-image: initial; vertical-align: bottom" width="100px;"><b>�������</b></td>
        <td align="center" valign="middle" style="border-image: initial;" rowspan="2" width="80px;">&nbsp;<img src="/upload/news/ik2.svg" style="min-width: 125%;" width="20px;" /></td>
        <td style="border-image: initial;vertical-align: bottom" valign="bottom" width="100px;"><b>������</b></td>
        <td align="center" valign="middle" style="border-image: initial;" rowspan="2" width="80px;">&nbsp;<img src="/upload/news/ik3.svg" style="min-width: 125%;" width="20px;" /></td>
        <td style="border-image: initial;vertical-align: bottom" valign="bottom" width="150px;"><b>��������� ��������</b></td>
    </tr>
   
    <tr> <td style="border-image: initial;" valign="top"><span style="text-transform: uppercase;"><b>349 ���� ���.</b></span></td> <td style="border-image: initial;" valign="top"><span style="text-transform: uppercase;"><b>3 385 ���� ���.</b></span></td><td style="border-image: initial;" valign="top"><span style="text-transform: uppercase;"><b>1 767 ���� ���.</b></span></td> </tr>
   </tbody>
 </table>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>